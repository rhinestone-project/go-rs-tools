package main

import (
	"fmt"
	"go-rstool/autover"
	"os"
)

func printHelp() {
	fmt.Println("supported tools:")
	fmt.Println("  autover - Increase build number for specified .go file")
}

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		fmt.Println("error: tool not selected")
		printHelp()
		os.Exit(1)
	} else {
		switch args[0] {
		case "autover":
			autover.Execute(args[1:])

		default:
			fmt.Println("unsupported tool \"" + args[0] + "\"")
			printHelp()
			os.Exit(1)
		}
	}
}
