package autover

import (
	"go/ast"
	"go/parser"
	"go/printer"
	"go/token"
	"io"
	"os"
	"path/filepath"
	"strconv"
)

func readFile(path string) (*ast.File, *token.FileSet, error) {
	in, err := os.Open(path)
	if err != nil {
		return nil, nil, err
	}
	defer func(c io.Closer) {
		if err := c.Close(); err != nil {
			println("readFile() error:", err.Error())
		}
	}(in)

	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, filepath.Base(path), in, parser.AllErrors)
	return f, fset, err
}

func increment(node ast.Node) (bool, int) {
	switch n := node.(type) {
	case *ast.BasicLit:
		if n.Kind == token.INT {
			i, err := strconv.Atoi(n.Value)
			if err != nil {
				println("increment() error:", err.Error())
				return false, -1
			}
			i++
			n.Value = strconv.Itoa(i)
			return false, i
		}
	}
	return true, -1
}

func autoIncrementBuildNumber(f *ast.File) (number int) {
	number = -1
	ast.Inspect(f, func(node ast.Node) bool {
		switch n := node.(type) {
		case *ast.GenDecl:
			for _, spec := range n.Specs {
				switch s := spec.(type) {
				case *ast.ValueSpec:
					for idx, name := range s.Names {
						if name.Name == "buildNumber" {
							ast.Inspect(s.Values[idx], func(node ast.Node) bool {
								a, b := increment(node)
								number = b
								return a
							})
							return false
						}
					}
				}
			}
		}
		return true
	})
	return
}

func saveFile(path string, f *ast.File, fset *token.FileSet) error {
	out, err := os.OpenFile(path, os.O_WRONLY, 0666)
	if err != nil {
		return err
	}
	defer func(c io.Closer) {
		if err := c.Close(); err != nil {
			println("saveFile() error:", err.Error())
		}
	}(out)

	if err := printer.Fprint(out, fset, f); err != nil {
		return err
	}
	return nil
}

func execute(file string, silent bool) int {
	file, err := filepath.Abs(file)
	if err != nil {
		println("execute() error:", err.Error())
		return 1
	}

	f, fset, err := readFile(file)
	if err != nil {
		println("execute() error:", err.Error())
		return 2
	}

	buildNumber := autoIncrementBuildNumber(f)
	if buildNumber == -1 {
		println("execute() error: build number not incremented")
		return 3
	} else if !silent {
		println("build number:", buildNumber)
	}

	err = saveFile(file, f, fset)
	if err != nil {
		println("execute() error:", err.Error())
		return 4
	}

	return 0
}

func printHelp() {
	println("usage: <program> [-s|--silent] -f|--file <path/to/version_file.go>")
}

func Execute(args []string) {
	if len(args) == 0 {
		printHelp()
	} else {
		var file string
		silent := false
		for i := 0; i < len(args); i++ {
			switch args[i] {
			case "-f", "--file":
				i++
				if i < len(args) {
					file = args[i]
				} else {
					printHelp()
					return
				}
			case "-s", "--silent":
				silent = true
			}
		}

		code := execute(file, silent)
		if code != 0 {
			os.Exit(code)
		}
	}
}
